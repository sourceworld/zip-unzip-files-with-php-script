
# ZIP/UNZIP Files with PHP Script

```php

<?php

#ZIP PROGRESS

$zip = new ZipArchive();

$files_to_zip = array("file1.ext","file2.ext");

$zip_file_name = rand().".zip";

if ($zip->open($zip_file_name, ZIPARCHIVE::CREATE) == TRUE) {
        echo ("Ziped Successfully!");
}

    foreach ($files_to_zip as $file) {
      $zip->addFile($file);
    }

$zip->close();

?>

<?php

#UNZIP PROGRESS

$zip_to_unzip = "zip_file_here.zip";
$path = pathinfo(realpath($zip_to_unzip), PATHINFO_DIRNAME);

$zip = new ZipArchive;


if ($zip->open($zip_to_unzip) === TRUE) {

    $zip->extractTo($path);
    $zip->close();
    echo "File ".$zip_to_unzip." Unzipped";
    
}else {
    
    echo "Failed to Unzip ".$zip_to_unzip;
    
}

?>
```